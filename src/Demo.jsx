import React from 'react';

class Demo extends React.Component {
    render(){
        
        return (
        <div>
            <h1>Demo Component</h1>
            <p>This is my first componet. I am {this.props.name}</p>
            <p>{this.props.children}</p>
        </div>    
        )
    }
}

export default Demo;

