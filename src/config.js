export const CONFIG = {

    CURRENCY:'INR'
}

export const API_URL = {

    PRODUCT : 'https://raw.githubusercontent.com/mdmoin7/Random-Products-Json-Generator/master/products.json',
    CURRENCY : 'https://api.exchangeratesapi.io/latest?base=INR'
}

