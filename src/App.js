import React from 'react';
import logo from './logo.svg';
import './App.css';
import Demo from './Demo';
import ProductList from './containers/ProductList'
import Currency from './components/Currency';
import CartContextProvider from './containers/CartContextProvider';
import ShoppingCart from './containers/ShoppingCart';
import { CONFIG } from './config';
import Checkout from './containers/Checkout';
import Header from './containers/Header';
import AppRouter from './routes';
import { BrowserRouter as Router } from 'react-router-dom';

class App extends React.Component {

  // class - stateful - SMART
  // function - stateless - DUMB
  state = { currentCurrency: CONFIG.CURRENCY };

  render() {
    let { currentCurrency } = this.state;
    return (
      <Router>
        <div className="App">

          {/* <Demo name='Yash'>I am a Coder</Demo>
      <Currency currencyChange={(e) => this.setState({currentCurrency:e})}/> */}
          <CartContextProvider>
            <Header>
              <Currency currencyChange={(e) => this.setState({ currentCurrency: e })} />
            </Header>
            <AppRouter />
            {/*<ShoppingCart currencyCode = {currentCurrency}/>
              <Checkout/>
            <ProductList currencyCode = {currentCurrency}></ProductList> */}
          </CartContextProvider>
        </div>
      </Router>
    );
  }

}

export default App;
