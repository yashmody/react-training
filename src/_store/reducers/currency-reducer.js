import { CONFIG } from "../../config";
import { UPDATE_CURRENCY } from "../actions/currency-action";


function currencyReducer(state = CONFIG.CURRENCY, action) {
    switch (action.type) {
        case UPDATE_CURRENCY:
            return action.code;
        default:
            return state;
    }
}

export default currencyReducer;