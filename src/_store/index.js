import {createStore} from 'redux';
import currencyReducer from './reducers/currency-reducer';

const appStore = createStore(currencyReducer);

export default appStore;