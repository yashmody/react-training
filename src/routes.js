import React from 'react';
import {Route,Switch} from 'react-router-dom';
import Demo from './Demo';
import ProductList from './containers/ProductList';
import ErrorPage from './components/ErrorPage';

function AppRouter(){
    return(
        <Switch> 
            <Route path="/" component={Demo} exact/>
            <Route path="/products" component={ProductList}/>
            <Route component={ErrorPage}/>
        </Switch>

    );

}

export default AppRouter;