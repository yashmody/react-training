
import * as _ from 'lodash';


function addToCart(cart,item){
    const index = findProductById(cart,item.productId);
    console.log('adding index ',index);
    if( index < 0){
        return addItem(cart,item);
    }
    cart[index].qty += 1;
    return [...cart];
}

function findProductById(cart,id){
 return _.findIndex(cart, (c) => c.productId === id);
}


function addItem(cart,item){
    return [...cart,{...item,qty:1}];
}

function removeItem(cart,id){
    return cart.filter(p => p.productId !== id);
}

function cartTotal(cart){
    _.sumBy(cart,(c) => c.productPrice * c.qty);
}

export {cartTotal,addToCart,removeItem};