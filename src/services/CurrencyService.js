import { API } from "../core/ApiService";
import { API_URL, CONFIG } from "../config";
import * as _ from "lodash";

function getForexData() {
    return API.get(API_URL.CURRENCY).then(
        (res) => { cacheForexData(res.data.rates)
            return Promise.resolve(res.data.rates);
        }).catch( err => Promise.reject(err))
}


function cacheForexData(rates) {
    if(localStorage){
        console.log('caching', rates);
        const data = JSON.stringify(rates);
        localStorage.setItem('currency',data);
    }

}
function getCacheData(){
    if(localStorage.getItem('currency')){
        const data = JSON.parse(localStorage.getItem('currency'));
        return data;
    }
    return null;
}
function getCurrencyCodes(){
    const rates = getCacheData();

    return new Promise((resolve,reject) => {
        if(rates !== null ){
            resolve(Object.keys(rates));
        }else{
            getForexData().then(
                data => resolve(Object.keys(data))
            ).catch(
                err => reject(err)
            );
        }
    }); 

}

function convertValue(price, code = CONFIG.CURRENCY){
    const rates = getCacheData();
    if(rates != null){
        const converted = price * rates[code];
        return `${code} ${converted.toFixed(2)}`;
    }

    return `${code} ${price.toFixed(2)}`;

}

export {getForexData,convertValue,getCurrencyCodes};



let CURRENCY_DATA = {};

export function getCurrencyData(){
    console.log("getting data");
    if(_.isEmpty(CURRENCY_DATA) || currencyDataExpired(CURRENCY_DATA.date)){
        console.log("calling API");
        API.get(API_URL.CURRENCY).then((res) => {
            console.log('resp', res.data);
            CURRENCY_DATA=res.data;
            console.log('currency',CURRENCY_DATA);
        });
    }
    return CURRENCY_DATA;
}

function currencyDataExpired(currencyDate){
    const now = new Date();
    now.setDate(now.getDate() - 1);
    const dataDate = new Date(currencyDate);
    return (now.getTime() > dataDate.getTime());
}