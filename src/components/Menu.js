import React from 'react';
import {Link} from 'react-router-dom';

function Menu(props) {

    return (
        <ul class="navbar-nav mr-auto">
            {
                props.menuList.map(item =>
                    <li class="nav-item active" key={item.menuText}>
                        <Link class="nav-link" to={item.menuLink}>{item.menuText}</Link>
                    </li>
                )
            }
        </ul>
    );
}

export default Menu;