import React from 'react';

function ErrorPage(props){
    return(
        <div className="text-center">
            <h1>404 Error</h1>
        </div>
    )
}

export default ErrorPage;