import React,{Component} from 'react';
import { getCurrencyCodes, convertValue } from '../services/CurrencyService';
import { CONFIG } from '../config';


class Currency extends Component{
    
    state = {currencies : [],currentCurrency:CONFIG.CURRENCY};

    componentDidMount(){
        getCurrencyCodes().then(
            codes => this.setState({currencies:codes}) 
        ).catch(
            err => console.log(err)
        );
    }

    render(){
        const {currencies} = this.state;
        return (
        <select value={this.state.currentCurrency} onChange={(e)=>{
            this.setState({currentCurrency:e.currentTarget.value});
            this.props.currencyChange(e.currentTarget.value)
            
        }}>
            {
            currencies.map( c => <option key={c}>{c}</option>)
        }
        </select>)
        ;
    }

}

export default Currency;