import React, { Component } from 'react';
import CartContextProvider, { CartContext } from '../containers/CartContextProvider';
import { convertValue } from '../services/CurrencyService';
import Column from './Column';
import PropTypes from 'prop-types';

class Product extends Component {
    
    // PropTypes allow props to be enforced during compile time 

    static propTypes = {
         pData:PropTypes.object.isRequired,
         wishlist:PropTypes.bool
     }

    //  Default value of props in case they are missing
     static defaultProps = {
         wishlist: true
     }

     // Selective Rendering

     shouldComponentUpdate(nextProps,nextState){
         console.log(nextProps,nextState);
         return false;
     }

    renderStock() {
        const { pData, wishlist } = this.props

        if (pData.productStock) {


            return (
                <CartContext.Consumer>
                    {(context) => (
                        <button className='btn btn-primary' onClick={() => context.addToCart(pData)}>Add to {wishlist ? 'Wishlist' : 'Cart'}</button>
                    )}
                </CartContext.Consumer>);

        }
        return <p>Out of Stock</p>;

    }

    render() {

        const { pData, currency } = this.props;

        return (
            <Column size={4} >
                <div className="card bg-light mb-3">
                    <img src={pData.productImage} />
                    <h3>{pData.productName}</h3>
                    <h4>{convertValue(pData.productPrice, currency)}</h4>

                    {this.renderStock()}

                    {/*
                    pData.pStock ?
                     <button>Add to Cart</button> :
                     <p>Out of Stock</p>
                */}
                </div>
            </Column>
        );
    }

}

// Product.prototypes = {}
export default Product

