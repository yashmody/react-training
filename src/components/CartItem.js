//hooks
import React, { useState } from 'react';
import { convertValue } from '../services/CurrencyService';

function CartItem(props) {
    const { item } = props;

    let [qty, setsQty] = useState(1);

    return (
        <tr key={item.pID}>
            <td>{item.productName}</td>
            <td>{props.currentCurrency} {item.productPrice}</td>
            <td><input type="number" value={item.qty} onChange={
                (e) => setsQty(e.currentTarget.value)
            }></input></td>
            <td>{convertValue(item.qty * item.productPrice, props.currency)}</td>
            <td><button>x</button></td>
        </tr>
    );


}

export default CartItem;
