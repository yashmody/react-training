import React from 'react';

function Column(props){
    return(
        <div className={`col-md-${props.size}`}>{props.children}</div>
    );
}
// Add propstype here with same syntax

// Columns.propType

export default Column;