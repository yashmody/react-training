import React, { Component } from 'react';
import { API } from '../core/ApiService';
import Menu from '../components/Menu';

class Header extends Component {

    state = { menuItems: [] }

    componentDidMount() {
        API.get('/assets/menu.json').then(
            res => this.setState({ menuItems: res.data })
        ).catch(err => console.log(err))
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#">Navbar</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <Menu menuList={this.state.menuItems} />
                    {this.props.children}
                </div>
            </nav>
        );
    }

}

export default Header;

