import React, {Component} from 'react'
import { addToCart } from '../services/CartService';

export const CartContext = React.createContext();

class CartContextProvider extends Component {

    state = {
        cart: []
    }

    addNewItem(p){
        const cart = addToCart(this.state.cart,p);
        this.setState({cart});
    }
    render() { 
        return (
            <CartContext.Provider value= {
                {
                    state:this.state,
                    addToCart: (product) => this.addNewItem(product)
                }
            }>
                    {this.props.children} 
            </CartContext.Provider>

        );
    }
}

export default CartContextProvider;