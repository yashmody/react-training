import React, { Component } from 'react';
import { CartContext } from './CartContextProvider';
import CartItem from '../components/CartItem';


class ShoppingCart extends Component {

    state = {};

    render() {

        return (<CartContext.Consumer>

            {(context) => (<div><table border="1"> {context.state.cart.map(item =>

                <CartItem item={item} currency={this.props.currentCurrency} />



            )}</table></div>)}

        </CartContext.Consumer>);

    }


}

export default ShoppingCart;