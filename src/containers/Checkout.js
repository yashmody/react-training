import React,{Component} from 'react';


class Checkout extends Component{

    state = {name:''};
    
    emailRef = null;

    submitData(e){
        e.preventDefault();
        console.log("Submitted", this.state.name, this.emailRef.value);
    }

    render(){
        return (
        <form onSubmit={(e) => this.submitData(e)}>
            <label>Name</label>
            <input type="text" value={this.state.name} onChange={
                e => this.setState({name:e.currentTarget.value})
            }></input>

            <label>Email</label>
            <input ref={r => this.emailRef = r}></input>

            <button>Submit</button>
            
        </form>
        );
    }

}

export default Checkout;
