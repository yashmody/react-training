import React, { Component } from 'react';
import Product from '../components/Products';
import { getProducts } from '../services/ProductService';

class ProductList extends Component {

    state = { prodData: [] }

    componentDidMount() {
        console.log('mount');
        this.getData();
    }

    getData() {
        getProducts().then(
            res => {
                console.log(res.data)
                return this.setState({ prodData: res.data })
            })
            .catch(
                err => console.log(err));
    }

    render() {
        console.log('render');
        const { prodData } = this.state;

        return (
            <div className="row">
                {
                    prodData.map(p => <Product pData={p} currency={this.props.currencyCode} key={p.productId} />)
                }
            </div>

        );
    }

}

export default ProductList;