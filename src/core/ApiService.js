import axios from 'axios';

function getData(api,config={}){
    return axios.get(api,config).then(res => ({satus:res.status, data:res.data}));
}

function postData(api, data, config={}){
    return axios.put(api,data, config)
}

function putData(api,data, config={}){
    return axios.put(api,data, config)
}

function deleteData(api,config={}){
    return axios.delete(api,config)
}

export const API = {
    get:getData,
    post:postData,
    put:putData,
    delete:deleteData
}

